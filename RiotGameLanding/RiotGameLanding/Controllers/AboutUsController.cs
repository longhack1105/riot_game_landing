﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RiotGameLanding.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RiotGameLanding.Controllers
{
    public class AboutUsController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public AboutUsController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
